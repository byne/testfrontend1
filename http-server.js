const express = require("express");
const fs = require("fs");
const app = express();
const port = 3000;

const generatePriceData = (numberOfMinutes) => {
  let startingPrice = Number(Math.floor(Math.random() * 1000) + 1);
  let marketWeighting = Number(Math.random() * 2) + -1;
  let volatility = Number(Math.random() * 10) + 1;
  let priceDataArray = [];
  for (let x = 0; x < numberOfMinutes; x++) {
    let maxMovement = Number(
      startingPrice * 0.1 + volatility * marketWeighting
    );
    priceDataArray.push({ minute: x, price: Number(startingPrice.toFixed(2)) });
    let movement = Number(maxMovement * Math.random());
    movement = Number(Math.random() < 0.5)
      ? -Math.abs(movement)
      : Math.abs(movement);
    let significantMovement = Number(Math.random() < 0.01) ? 10 : 1;
    startingPrice = Number(startingPrice + movement * significantMovement);
  }
  return priceDataArray;
};

app.get("/api/stocks", (req, res) => {
  try {
    const stocks = JSON.parse(fs.readFileSync("stocks.json"));

    const transformedStocks = stocks.map((stock) => {
      const { basePrice, ...stockWithoutBasePrice } = stock;
      return {
        ...stockWithoutBasePrice,
        priceData: generatePriceData(5),
      };
    });

    res.json(transformedStocks);
  } catch (error) {
    res.status(500).json({ error: "Could not load stocks data" });
  }
});

app.listen(port, () => {
  console.log(`HTTP server is running on port ${port}`);
});
